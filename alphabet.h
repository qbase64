#ifndef ALPHABET_H
#define ALPHABET_H
  /** 
   * @def ALPHABET_SIZE 
   * alphabet size (in base64 encoding this is \e equal to 64)
   *
   */
  #define ALPHABET_SIZE 64
  
  /**
   * @namespace base64
   * groups base64 manipulation methods and objects
   *
   */
  namespace base64
  {
    static const char alphabet[ALPHABET_SIZE] =
    {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M' ,'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 
    'g', 'h', 'i', 'j', 'k', 'l', 'm' ,'n', 
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'
    };   
  };
#endif
